import tkinter as tk
import webbrowser

# The website that will you'll be redirected to, change to what you like
URL = "https://youtube.com"

# Different channels that you can click on
def open_yt(event):
	webbrowser.open_new_tab("https://youtube.com")

def open_channel(event):
	webbrowser.open_new_tab(URL + "/channel/UCyfG7-7lPhf6EIC6igUOfzw")

def open_channel_1(event):
	webbrowser.open_new_tab(URL + "/channel/UC9CuvdOVfMPvKCiwdGKL3cQ")

# Similar to CSS in HTML this just labels the buttons and positions them
window = tk.Tk()
window.geometry("300x200")

alabel = tk.Label(text="Home")
alabel.grid(column=1, row=0)

blabel = tk.Label(text="Channel")
blabel.grid(column=2, row=0)

clabel = tk.Label(text="Channel")
clabel.grid(column=3, row=0)

button = tk.Button(window, text="youtube")
button.grid(column=1, row=1)

button2 = tk.Button(window, text="TFS")
button2.grid(column=2, row=1)

button3 = tk.Button(window, text="GameGrumps")
button3.grid(column=3, row=1)

button.bind("<Button-1>", open_yt)
button2.bind("<Button-1>", open_channel)
button3.bind("<Button-1>", open_channel_1)

# Keeps the window open instead of instantly closing on execution
window.mainloop()
