# import urllib.request as ur
from bs4 import BeautifulSoup as bs
from requests_html import HTMLSession
import time
import os

shows = {}

userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 /(KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
headers = {"User-Agent": userAgent}
url = "http://kshowonline.com"
session = HTMLSession()

# Tries getting the whole site
def Ripit():
    try:
            # Go to the page where all the shows are listed
        r = session.get("http://kshowonline.com")
        r2 = session.get("http://kshowonline.com/show-list")

        # Start scraping all the links
        soup = bs(r2.content, "html.parser")

        # Get the element with all the shows and from that get all the individual shows <li> 
        showlist = soup.find("ul", {"class": "showlist"})
        showlis = showlist.find_all("li")

        # For all the shows found in the list get the title and link
        for show in showlis:
           
           showa = show.find("a")
           showname = showa.get("title")
           showlink = showa.get("href")
           shows[showname] = showlink
           #print(f"{showlink} and {showname}")
        
        print("[*] Got all the show links and names")
    except Exception as e:
        print(f"[!] Startup for downlaoding everything failed with: {e}")
    

def DownloadEpisodePage(name, link):
    # Creates a new dict for all the episodes with their links
    episodes = {}

    #try:
    # Gets the div and the As of the shows from the website
    showresponse = session.get(link)
    showPage = bs(showresponse.content, "html.parser")
    EpisodeDiv = showPage.find("div", {"class": "content-list z-depth-1"})
    episodelist = EpisodeDiv.find_all("a")

    # Gets all the episode names with their urls
    for episode in episodelist:
        episodetitle = episode.get("title")
        episodeurl = episode.get("href")
        episodes[episodetitle] = episodeurl

    # Makes a new directory for the show if it doesn't exist
    print("Got episde page links and names")
    if not os.path.exists(name):
        os.mkdir(name)

    for title, url in episodes.items():
        print(f"starting downdload of {title}\n\n\n")
        print(name, title, url)
        downloadVideo(name, title, url, session)

    #except Exception as e:
     #   print(f"Downlaoding failed with error: {e}")

def downloadVideo(path, title, url, session):
    htmlObject = session.get(url)
    episodeSoup = bs(htmlObject.content, "html.parser")

    videoTag = episodeSoup.find("video")
    if videoTag.get("id") == "olvideo_html5_api":
        fullUrl = f"https://oload.stream{videoTag.get('src')}"
        print(fullUrl) 
        time.sleep(25)


def GetEverything():
    Ripit()
    time.sleep(4.6)

    for k, v in shows.items():
        while True:
            DownloadEpisodePage(k, v)
        time.sleep(25)

if __name__ == "__main__":
    GetEverything()


